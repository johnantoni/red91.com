[![volkswagen status](https://auchenberg.github.io/volkswagen/volkswargen_ci.svg?v=1)](https://github.com/auchenberg/volkswagen)

### Install

    bundle install
    npm install -g grunt grunt-cli
    npm install

#### Build pages

    grunt build

#### Build & deploy pages

    grunt build
    grunt buildcontrol:pages

#### Copyright

Copyright (c) 2014 John Griffiths. See [LICENSE](LICENSE) for details.
